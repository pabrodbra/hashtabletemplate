/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Blinsky
 *
 * Created on 12 de junio de 2017, 21:02
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#include "hash.h"

/*
 * g++ hash.cpp main.cpp -o test.exe
 */
int main(int argc, char** argv) {

    hash hashTest;
    
    string searchName = "Kim";
    
    hashTest.insertItem("Paul", "Locha");
    hashTest.insertItem("Kim", "Iced Mocha");
    hashTest.insertItem("Emma", "Strawberry Smoothie");
    hashTest.insertItem("Annie", "Hot Chocolate");
    hashTest.insertItem("Sarah", "Passion Tea");
    hashTest.insertItem("Pepper", "Caramel Mocha");
    hashTest.insertItem("Mike", "Chai Tea");
    hashTest.insertItem("Steve", "Apple Cider");
    hashTest.insertItem("Bill", "Root Beer");
    hashTest.insertItem("Marie", "Skinny Latte");
    hashTest.insertItem("Susan", "Water");
    hashTest.insertItem("Joe", "Green Tea");
    
    //hashTest.printTable();
    hashTest.printItemsInIndex(2);
    
    while(searchName != "exit"){
        cout << "Remove: ";
        cin >> searchName;
        if (searchName != "exit")
            hashTest.removeItem(searchName);
    }
    
    //hashTest.printTable();
    hashTest.printItemsInIndex(2);
    
    return 0;
}

