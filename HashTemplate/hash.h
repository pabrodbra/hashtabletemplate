/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hash.h
 * Author: Blinsky
 *
 * Created on 12 de junio de 2017, 21:11
 */

#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#ifndef HASH_H
#define HASH_H

#ifdef __cplusplus
extern "C" {
#endif

class hash{
private:
    static const int tableSize = 4;
    
    struct item{
        string name;
        string food;
        item * next;
    };
    
    item * hashTable[tableSize];
public:
    hash();
    int computeHash(string key);
    
    void insertItem(string name, string food);
    void removeItem(string name);
    
    int numberOfItemsInIndex(int index);
    
    void printTable();
    void printItemsInIndex(int index);
    
    void findFood(string name);
};


#ifdef __cplusplus
}
#endif

#endif /* HASH_H */

