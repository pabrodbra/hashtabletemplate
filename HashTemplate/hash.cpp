/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   hash.cpp
 * Author: Blinsky
 * 
 * Created on 12 de junio de 2017, 21:06
 */
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#include "hash.h"

hash::hash(){
    for(int i = 0; i < tableSize; i++){
        hashTable[i] = new item;
        hashTable[i]->name = "null";
        hashTable[i]->food = "null";
        hashTable[i]->next = NULL;
    }
}

void hash::insertItem(string name, string food) {
    int index = computeHash(name);
    
    if(hashTable[index]->name == "null"){
        hashTable[index]->name = name;
        hashTable[index]->food = food;
    } else {
        item* pointer = hashTable[index];
        item* newItem = new item;
        newItem->name = name;
        newItem->food = food;
        newItem->next = NULL;
        while(pointer->next != NULL){
            pointer = pointer->next;
        }
        pointer->next = newItem;
    }
}

void hash::removeItem(string name){
    int index = computeHash(name);
    
    item* delPtr;
    item* p1;
    item* p2;
    
    // 0) Bucket is empty
    if(hashTable[index]->name == "null" && hashTable[index]->food == "null"){
        cout << name << " was not found! Cannot remove!\n";
    }
    
    // 1) Only 1 item contain and it matches
    else if(hashTable[index]->name == name){
        if(hashTable[index]->next == NULL){
            hashTable[index]->name = "null";
            hashTable[index]->food = "null";
        }else{
    // 2) Match is located in first item, but bucket has more
            delPtr = hashTable[index];
            hashTable[index] = hashTable[index]->next;
            delete delPtr;
        }
        cout << name << " has been removed from HashTable!" << endl;  
    }
    
    
    // 3) Bucket contains items but first item is not a match
    else{
        p1 = hashTable[index]->next;
        p2 = hashTable[index];
        
        while(p1 != NULL && p1->name != name){
            p2 = p1;
            p1 = p1->next;
        }
        // 3.1) No match
        if(p1 == NULL){
            cout << name << " was not found!\n";
        }
        // 3.2) Match found
        else{
            delPtr = p1;
            p1 = p1->next;
            p2->next = p1;
            
            delete delPtr;
            cout << name << " has been removed from HashTable!" << endl;
        }
    }
}

int hash::numberOfItemsInIndex(int index){
    int count = 0;
    
    if(hashTable[index]->name == "null"){
        return count;
    } else {
        count++;
        item * pointer = hashTable[index];
        while(pointer->next != NULL){
            count++;
            pointer = pointer->next;
        }
    }
    
    return count;
}

int hash::computeHash(string key){
    int hash = 0;
    int index;
    
    for(int i = 0; i < key.length(); i++){
        hash = hash + (int)key[i] * 33;
    }
    
    index = hash % tableSize;
    
    return index;
}

void hash::printTable(){
    int number;
    for(int i = 0; i < tableSize; i++){
        number = numberOfItemsInIndex(i);
        cout << "------------------------\n";
        cout << "index = " << i << endl;
        cout << hashTable[i]->name << endl;
        cout << hashTable[i]->food << endl;
        cout << "# of items = " << number << endl;
        cout << "------------------------\n";
    }
}

void hash::printItemsInIndex(int index){
    item * pointer = hashTable[index];
    
    if(pointer->name == "null"){
        cout << "index = " << index << " is empty\n";
    } else {
        cout << "index = " << index << " contains the following items\n";
        
        while (pointer != NULL){
            cout << "------------------------\n";
            cout << pointer->name << endl;
            cout << pointer->food << endl;
            cout << "------------------------\n";
            pointer = pointer->next;
        }
    }
}

void hash::findFood(string name){
    int index = computeHash(name);
    bool foundName = false;
    string food;
    item * ptr = hashTable[index];
    
    while(ptr != NULL){
        if(ptr->name == name){
            foundName = true;
            food = ptr->food;
        }
        ptr = ptr->next;
    }
    
    if(foundName == true)
        cout << "Favorite food = " << food << endl;
    else
        cout << name << "'s favorite food wasn't found!" << endl;
    
}